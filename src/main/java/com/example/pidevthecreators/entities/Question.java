package com.example.pidevthecreators.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Question implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;
    private String question;
    private String proposition1;
    private String proposition2;
    private String proposition3;
    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL)
    private List<Response> responses;

    @ManyToOne
    private Feedback feedback;


}
