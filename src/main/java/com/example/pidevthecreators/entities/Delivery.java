package com.example.pidevthecreators.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Delivery implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int RefDelivery;
    private float coastDelivery;
    private float totalCoast;
    private String adress;
    private StatusDelivery statusDelivery;
    private Date startDate;
    private Date endDate;
    @Enumerated(EnumType.STRING)
    private TypeDelivery typeDelivery;
    @OneToOne
    private Order order;

}
