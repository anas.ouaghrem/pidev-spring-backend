package com.example.pidevthecreators.repositories;

import com.example.pidevthecreators.entities.Product;
import com.example.pidevthecreators.entities.ProductRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRatingRepository extends JpaRepository<ProductRating,Long> {

    List<ProductRating> getProductRatingsByRatedProduct(Product product);
}
