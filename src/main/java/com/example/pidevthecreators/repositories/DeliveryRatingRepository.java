package com.example.pidevthecreators.repositories;

import com.example.pidevthecreators.entities.DeliveryRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryRatingRepository extends JpaRepository<DeliveryRating,Long> {

}
