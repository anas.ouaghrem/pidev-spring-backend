package com.example.pidevthecreators.controllers;

import com.example.pidevthecreators.entities.DeliveryRating;
import com.example.pidevthecreators.services.DeliveryRatingService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("delivery-rating")
@AllArgsConstructor
public class DeliveryRatingController {

    private final DeliveryRatingService deliveryRatingService;

    @GetMapping("/get-all-ratings")
    public List<DeliveryRating> retrieveAllDeliveryRatings() {
        return deliveryRatingService.getAllRatings();
    }

    @GetMapping("/get-rating/{ratingId}")
    public DeliveryRating getDeliveryRating(@PathVariable Long ratingId ) {
        return deliveryRatingService.getRatingById(ratingId);
    }

    @PostMapping("/add-rating")
    public DeliveryRating addDeliveryRating(@RequestBody DeliveryRating deliveryRating) {
        return deliveryRatingService.createDeliveryRating(deliveryRating);
    }

    @PutMapping("/update-rating/{rateId}/{rating}")
    public DeliveryRating updateDeliveryRating(@PathVariable Long rateId, @PathVariable int rating) {
        return deliveryRatingService.updateDeliveryRating(rateId,rating);
    }

    @DeleteMapping("/delete-delivery-rating/{rateId}")
    public void deleteDeliveryRating(@PathVariable Long rateId) {
        deliveryRatingService.deleteRating(rateId);
    }

}
