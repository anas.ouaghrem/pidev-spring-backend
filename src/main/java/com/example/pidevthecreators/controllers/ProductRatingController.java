package com.example.pidevthecreators.controllers;

import com.example.pidevthecreators.entities.ProductRating;
import com.example.pidevthecreators.services.ProductRatingService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("product-rating")
@AllArgsConstructor
public class ProductRatingController {
    private final ProductRatingService productRatingService;

    @GetMapping("/get-all-product-ratings")
    public List<ProductRating> retrieveAllProductRatings() {
        return productRatingService.getAllRatings();
    }

    @GetMapping("/get-product-rating/{ratingId}")
    public ProductRating getProductRating(@PathVariable Long ratingId ) {
        return productRatingService.getRatingById(ratingId);
    }

    @PostMapping("/add-product-rating")
    public ProductRating addProductRating(@RequestBody ProductRating productRating) {
        return productRatingService.createProductRating(productRating);
    }

    @PutMapping("/update-product-rating/{rateId}/{rating}")
    public ProductRating updateProductRating(@PathVariable Long rateId, @PathVariable int rating) {
        return productRatingService.updateDeliveryRating(rateId,rating);
    }

    @DeleteMapping("/delete-product-rating/{rateId}")
    public void deleteProductRating(@PathVariable Long rateId) {
        productRatingService.deleteRating(rateId);
    }

}
