package com.example.pidevthecreators.services;

import com.example.pidevthecreators.entities.Delivery;
import com.example.pidevthecreators.entities.DeliveryRating;
import com.example.pidevthecreators.repositories.DeliveryRatingRepository;
import com.example.pidevthecreators.repositories.DeliveryRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class DeliveryRatingService {

    private final DeliveryRatingRepository deliveryRatingRepository;
    private final DeliveryRepository deliveryRepository;

   public DeliveryRating createDeliveryRating(DeliveryRating deliveryRating) {
       return deliveryRatingRepository.save(deliveryRating);
   }

   public DeliveryRating updateDeliveryRating(Long rateId, int rating){
       DeliveryRating deliveryRating = deliveryRatingRepository.findById(rateId).orElseThrow(() -> new RuntimeException("Rating not found"));
       deliveryRating.setRating(rating);
       deliveryRating.setRatingDate(new Date());
       return deliveryRatingRepository.save(deliveryRating);
   }

    public DeliveryRating getRatingById(Long deliveryRatingId) {
       return deliveryRatingRepository.findById(deliveryRatingId).orElseThrow(()-> new RuntimeException("Rating not found"));
    }

    public List<DeliveryRating> getAllRatings(){
       return deliveryRatingRepository.findAll();
    }

    public void deleteRating(Long rateId){
       deliveryRatingRepository.deleteById(rateId);
    }
}
