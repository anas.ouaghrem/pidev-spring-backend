package com.example.pidevthecreators.services;

import com.example.pidevthecreators.entities.DeliveryRating;
import com.example.pidevthecreators.entities.Product;
import com.example.pidevthecreators.entities.ProductRating;
import com.example.pidevthecreators.repositories.ProductRatingRepository;
import com.example.pidevthecreators.repositories.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class ProductRatingService {
    private final ProductRatingRepository productRatingRepository;
    private final ProductRepository productRepository;

    public ProductRating createProductRating(ProductRating productRating) {
        productRatingRepository.save(productRating);
        updateProductRating(productRating.getRatedProduct().getId());
        return productRating;
    }

    public ProductRating updateDeliveryRating(Long rateId, int rating){
        ProductRating productRating = productRatingRepository.findById(rateId).orElseThrow(() -> new RuntimeException("Rating not found"));
        productRating.setRating(rating);
        productRating.setRatingDate(new Date());
        productRatingRepository.save(productRating);
        updateProductRating(productRating.getRatedProduct().getId());
        return productRating;
    }

    public ProductRating getRatingById(Long productRatingId) {
        return productRatingRepository.findById(productRatingId).orElseThrow(()-> new RuntimeException("Rating not found"));
    }

    public List<ProductRating> getAllRatings(){
        return productRatingRepository.findAll();
    }

    public void deleteRating(Long rateId){
        productRatingRepository.deleteById(rateId);
    }

    private void updateProductRating(int id){
        int sum = 0;
        int average = 0;
        Product product = productRepository.findById(id).orElseThrow(() -> new RuntimeException("No product found"));
        List<ProductRating> ratings = productRatingRepository.getProductRatingsByRatedProduct(product);
        for(ProductRating rating : ratings){
            sum = sum + rating.getRating();
            product.setNumberOfRates(product.getNumberOfRates() + 1);
        }
        product.setRating(sum/product.getNumberOfRates());
        productRepository.save(product);
    }
}
